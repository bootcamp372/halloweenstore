﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace HalloweenStore
{
    public class Candy : InventoryItem
    {
        public string Brand { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }
        public int ItemsPerPack { get; set; }
        public Candy(int id, string category, string name, string desc, decimal price, int quantity, string brand, string type, string size, int itemsPerPack)
            : base(id, category, name, desc, price, quantity)
        {
            Brand = brand;
            Type = type;
            Size = size;
            ItemsPerPack = itemsPerPack;
        }

        public override void DisplayDetails()
        {
            Console.WriteLine($"{Id} - {Name} - Category: {Category}");
            Console.WriteLine($"Desc:{Desc}");
            Console.WriteLine($"Price: {Price}");
            Console.WriteLine($"Quantity: {Quantity}");
            Console.WriteLine($"Brand: {Brand}");
            Console.WriteLine($"Type: {Type}");
            Console.WriteLine($"Size: {Size}");
            Console.WriteLine($"Items per pack: {ItemsPerPack}");
        }
    }
}
