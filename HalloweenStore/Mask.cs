﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HalloweenStore
{
    public class Mask : InventoryItem
    {
        public string Genre { get; set; }
        public string Material { get; set; }
        public string Size { get; set; }
        public Mask(int id, string category, string name, string desc, decimal price, int quantity, string genre, string material, string size)
            : base(id, category, name, desc, price, quantity)
        {
            Genre = genre;
            Material = material;
            Size = size;
        }
        public override void DisplayDetails()
        {
            Console.WriteLine($"{Id} - {Name} - Category: {Category}");
            Console.WriteLine($"Desc:{Desc}");
            Console.WriteLine($"Price: {Price}");
            Console.WriteLine($"Quantity: {Quantity}");
            Console.WriteLine($"Genre: {Genre}");
            Console.WriteLine($"Material: {Material}");
            Console.WriteLine($"Material: {Size}");
        }
    }
}