﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HalloweenStore
{
    public abstract class InventoryItem
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }

        public InventoryItem(int id, string category, string name, string desc, decimal price, int quantity)
        {
            this.Id = id;
            this.Category = category;
            this.Name = name;
            this.Desc = desc;
            this.Price = price;
            this.Quantity = quantity;
        }
        public abstract void DisplayDetails();
        public string ToString()
        {
            return $"{Id} - {Name} - Category: {Category}";
        }
    }
}
