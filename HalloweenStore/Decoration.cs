﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HalloweenStore
{
    public class Decoration : InventoryItem
    {
        public string Type { get; set; }
        public string Theme { get; set; }
        public Decoration(int id, string category, string name, string desc, decimal price, int quantity, string type, string theme)
            : base(id, category, name, desc, price, quantity)
        {
            Type = type;
            Theme = theme;

        }
        public override void DisplayDetails()
        {
            Console.WriteLine($"{Id} - {Name} - Category: {Category}");
            Console.WriteLine($"Desc:{Desc}");
            Console.WriteLine($"Price: {Price}");
            Console.WriteLine($"Quantity: {Quantity}");
            Console.WriteLine($"Type: {Type}");
            Console.WriteLine($"Theme: {Theme}");
        }
    }
}
