﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace HalloweenStore
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<InventoryItem> inventoryItems = ReadFile();
            MainMenu(inventoryItems);

        }

        static void MainMenu(List<InventoryItem> inventoryItems) {
            if (inventoryItems.Count > 0)
            {
                foreach (var item in inventoryItems)
                {
                    string productItem = item.ToString();
                    Console.WriteLine(productItem);
                }
                Console.WriteLine("Choose an item number to learn more:");
                int selection = Convert.ToInt16(Console.ReadLine());
                DisplayDetails(inventoryItems, selection);
            }
            else
            {
                Console.WriteLine("No Items in List");
            }
        }

        static void DisplayDetails(List<InventoryItem> inventoryItems, int selection) {

            InventoryItem inventoryItem = (
                    from item in inventoryItems
                    where item.Id == selection
                    select item).FirstOrDefault();
            inventoryItem.DisplayDetails();
            Console.WriteLine("What would you like to do?");
            Console.WriteLine("1 - Purchase Item");
            Console.WriteLine("2 - Return to Main Menu");
            string menuInput = Console.ReadLine();
            switch (menuInput)
            {
                case "1":
                    if (inventoryItem != null) {
                        PurchaseItem(inventoryItem);
                    }                    
                    break;
                case "2":
                    MainMenu(inventoryItems);
                    break;
                default:
                    Console.WriteLine("Invalid input");
                    break;
            }
            /*return inventoryItem;*/
            /*            string supplierData = supplier.ToString();
                        Console.WriteLine(supplierData);*/
        }

        static void PurchaseItem(InventoryItem inventoryItem)
        {
/*            When they decide to purchase the item, create an invoice file, prompting the
    user for relevant details and including the item information in the invoice.
    Create constructors for the base and derived classes, as well as methods for
    reading from the file, displaying the items, and creating the invoice file.
    Implement exception handling where necessary.*/
        }

        static List<InventoryItem> ReadFile()
        {
            List<InventoryItem> inventoryItems = new List<InventoryItem>(); ;

            string fileName = @"C:\Academy\HalloweenStore\HalloweenStoreInventory.txt";
            StreamReader inputFile = null;
            decimal totalPay = 0;
            try
            {
                inputFile = new StreamReader(fileName);

                while (!inputFile.EndOfStream)
                {
                    string fileLine = inputFile.ReadLine();
                    string[] values = fileLine.Split(',');
                   if (values.Length > 0)
                    {
                        switch (values[1])
                        {
                            case "Mask":
                                Mask mask = new Mask(
                                    Convert.ToInt32(values[0]),
                                    values[1],
                                    values[2],
                                    values[3],
                                    Convert.ToDecimal(values[4]),
                                    Convert.ToInt16(values[5]),
                                    values[6],
                                    values[7],
                                    values[8]
                                );
                                inventoryItems.Add(mask);
                                break;
                            case "Candy":
                                Candy candy = new Candy(
                                    Convert.ToInt32(values[0]),
                                    values[1],
                                    values[2],
                                    values[3],
                                    Convert.ToDecimal(values[4]),
                                    Convert.ToInt16(values[5]),
                                    values[6],
                                    values[7],
                                    values[8],
                                    Convert.ToInt16(values[9])
                                );
                                inventoryItems.Add(candy);
                                break;
                            case "Costume":
                                Costume costume = new Costume(
                                    Convert.ToInt32(values[0]),
                                    values[1],
                                    values[2],
                                    values[3],
                                    Convert.ToDecimal(values[4]),
                                    Convert.ToInt16(values[5]),
                                    values[6],
                                    values[7],
                                    values[8]
                                );
                                inventoryItems.Add(costume);
                                break;
                            case "Decoration":
                                Decoration decoration = new Decoration(
                                    Convert.ToInt32(values[0]),
                                    values[1],
                                    values[2],
                                    values[3],
                                    Convert.ToDecimal(values[4]),
                                    Convert.ToInt16(values[5]),
                                    values[6],
                                    values[7]
                                );
                                inventoryItems.Add(decoration);
                                break;
                            case "Effect":
                                SpecialEffect effect = new SpecialEffect(
                                    Convert.ToInt32(values[0]),
                                    values[1],
                                    values[2],
                                    values[3],
                                    Convert.ToDecimal(values[4]),
                                    Convert.ToInt16(values[5]),
                                    values[6],
                                    values[7]
                                );
                                inventoryItems.Add(effect);
                                break;
                            default:
                                Console.WriteLine("Couldn't read category from file");
                                break;
                        }
                    }
                }
                

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error opening file: {ex.Message}");
            }
            finally
            {
                if (inputFile != null) inputFile.Close();
            }
            return inventoryItems;
        }
    }
}