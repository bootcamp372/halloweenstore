﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HalloweenStore
{
    public class SpecialEffect : InventoryItem
    {
        public string Effect { get; set; }
        public string Color { get; set; }
        public SpecialEffect(int id, string category, string name, string desc, decimal price, int quantity, string effect, string color)
            : base(id, category, name, desc, price, quantity)
        {
            Effect = effect;
            Color = color;

        }
        public override void DisplayDetails()
        {
            Console.WriteLine($"{Id} - {Name} - Category: {Category}");
            Console.WriteLine($"Desc:{Desc}");
            Console.WriteLine($"Price: {Price}");
            Console.WriteLine($"Quantity: {Quantity}");
            Console.WriteLine($"Effect: {Effect}");
            Console.WriteLine($"Color: {Color}");
        }
    }
}
